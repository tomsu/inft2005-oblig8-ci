import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    // Verifiser innholdet i lista med should()
    cy.get('#cart').should('contain','1 Stratos; 8 kr');
    cy.get('#cart').should('contain','4 Hubba bubba; 8 kr');
    cy.get('#cart').should('contain','5 Smørbukk; 5 kr');
    cy.get('#cart').should('contain','2 Hobby; 12 kr');
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});

//************************************************* */

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});


When(/^jeg sletter en vare$/, () => {
    // velger Hubba bubba for sletting
    cy.get('#product').select('Hubba bubba');
    // trykker på slett
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    // Sjekker at Hubba bubba ikke er i listen lenger
    cy.get('#cart').should('not.have.value','Hubba bubba');
});

//******************************************************* */

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});


When(/^jeg oppdaterer kvanta for en vare$/, () => {
    // velger Stratos for oppdatering
    cy.get('#product').select('Stratos');
    // endrer antall til 5
    cy.get('#quantity').clear().type('5');
    // lagrer nye verdien
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    // Sjekker at det nå er 5 Stratos i listen
    cy.get('#cart').should('contain','5 Stratos');
});