import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    // går inn på siden
    cy.visit('http://localhost:8080');
    //legger til varer
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
        cy.get('#goToPayment').click();
});


When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    // Brukeren fyller inn credentials
    cy.get('#fullName').clear().type("Donald Duck");
    cy.get('#address').clear().type("Gagg-Gakk gaten 13");
    cy.get('#postCode').clear().type("3113");
    cy.get('#city').clear().type("Andeby");
    cy.get('#creditCardNo').clear().type("1234123412341234").blur();
});

And(/^trykker på Fullfør kjøp$/, function () {
        cy.get('input[type=submit]').click();
});


Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    // Sjekker bekreftelsesmelding på receipt.html
    cy.get('.confirmation').should('contain','Din ordre er registrert.');
});


//************************************************* */

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    // går inn på siden
    cy.visit('http://localhost:8080');
    //legger til varer
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
        cy.get('#goToPayment').click();
});


When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    // Tømmer alle input-felt (= ugyldig input)
    cy.get('input[type=text]').clear();
    //Setter fokus til knappen
    cy.get('input[type=submit]').focus();

});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    // sjekker alle feilmeldinger 
    cy.get('#fullNameError').should('contain', 'Feltet må ha en verdi');
    cy.get('#addressError').should('contain', 'Feltet må ha en verdi');
    cy.get('#postCodeError').should('contain', 'Feltet må ha en verdi');
    cy.get('#cityError').should('contain', 'Feltet må ha en verdi');
    cy.get('#creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer');
});

//******************************************************* */